// publicerror defines an errortype with a public and an internal/private
// error.
// The purpose of this package is to hide internal error information from the consumer
// while still being able to process that information internally
//
//	import (
//		pe "gitlab.com/Simerax/publicerror"
//	)
//
//	err := pe.Wrap(someInternalError, "This is some information for the public")
//
//	// some time later...
// 	if public, ok := err.(pe.PublicError); ok {
//		request.Respond(public.PublicError()) // send the public error to the client/consumer
//	} else {
//		// otherwise just tell them something went wrong
//		request.Respond("Internal server error")
//	}
//	// store the internal error somewhere else
//	log.Printf("my internal error: %s", err.Error())
//
//
package publicerror

import "fmt"

type PublicError interface {
	PublicError() string
}

// Causer is used to get the Internal Error of a Public Error.
// Not Every Public Error has to have a Causer.
//
// This interface is also designed to be compatible with github.com/pkg/errors
// "Cause" Function
type Causer interface {
	Cause() error
}

// WrappedPublicError is used to wrap an internal error with a public error
type WrappedPublicError interface {
	PublicError
	Causer
	Error() string
}

type publicError struct {
	err error
}

func (e publicError) PublicError() string {
	return e.err.Error()
}
func (e publicError) Error() string {
	return e.err.Error()
}

func (e publicError) Cause() error {
	return e.err
}

type publicStringError string

func (e publicStringError) Error() string {
	return string(e)
}
func (e publicStringError) PublicError() string {
	return string(e)
}

// MakePublic creates a PublicError for which both Error() and PublicError() return the same value
func MakePublic(msg string) error {
	return publicStringError(msg)
}

// MakePublicf is the same as MakePublic except that it's interface is like fmt.Sprintf
func MakePublicf(format string, args ...interface{}) error {
	return publicStringError(fmt.Sprintf(format, args...))
}

type publicWrappedError struct {
	publicErr PublicError
	cause     error
}

func (e publicWrappedError) PublicError() string {
	return e.publicErr.PublicError()
}

func (e publicWrappedError) Error() string {
	return e.cause.Error()
}

func (e publicWrappedError) Cause() error {
	return e.cause
}

// Wrap wraps an internal error with a public message.
// PublicError() will use msg
func Wrap(cause error, msg string) error {
	return publicWrappedError{
		publicErr: publicStringError(msg),
		cause:     cause,
	}
}

// Wrapf is the same as Wrap except that it's interface is like fmt.Sprintf
func Wrapf(cause error, format string, args ...interface{}) error {
	return publicWrappedError{
		publicErr: publicStringError(fmt.Sprintf(format, args...)),
		cause:     cause,
	}
}

// WrapPublic wraps an internal Error with a PublicError
func WrapPublic(cause error, public PublicError) error {
	return publicWrappedError{
		publicErr: public,
		cause:     cause,
	}
}

// WrapPublicErr uses public's Error() Function to satisfy the PublicError interface
func WrapPublicErr(cause error, public error) error {
	return WrapPublic(cause, publicError{err: public})
}

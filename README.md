# publicerror 
publicerror defines a public error and private errors wraped with public errors.  
The purpose of this package is to hide internal error information from the consumer while still being able to process that information internally by using the standard `error` interface

```go
import (
	pe "gitlab.com/Simerax/publicerror"
)

err := pe.Wrap(someInternalError, "This is some information for the public")

// some time later...
if public, ok := err.(pe.PublicError); ok {
	request.Respond(public.PublicError()) // send the public error to the client/consumer
} else {
	// otherwise just tell them something went wrong
	request.Respond("Internal server error")
}
// store the internal error somewhere else
log.Printf("my internal error: %s", err.Error())
```

package publicerror

import (
	"errors"
	"fmt"
	"testing"

	pkgerrors "github.com/pkg/errors"
)

func TestCompatibilityWithPkgErrors(t *testing.T) {
	err0 := errors.New("root error")
	err1 := Wrap(err0, "Public 1")
	err2 := pkgerrors.Wrap(err1, "test")

	cause := pkgerrors.Cause(err2)
	if cause.Error() != err0.Error() {
		t.Errorf("Expected root error to be '%s' but its '%s'", err0.Error(), cause.Error())
	}

}

func TestMakePublic(t *testing.T) {

	var tests = []struct {
		err       error
		publicStr string
	}{
		{
			err:       MakePublic("oh no!"),
			publicStr: "oh no!",
		},
		{
			err:       MakePublicf("test %d", 2),
			publicStr: "test 2",
		},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("%s", tt.publicStr)
		t.Run(testname, func(t *testing.T) {
			if tt.err.Error() != tt.publicStr {
				t.Errorf("err.Error() should be '%s' but it's '%s'", tt.publicStr, tt.err.Error())
			}

			public, ok := tt.err.(PublicError)
			if !ok {
				t.Errorf("err is not a PublicError!")
			}

			if public.PublicError() != tt.publicStr {
				t.Errorf("err.PublicError() should be '%s' but it's '%s'", tt.publicStr, public.PublicError())
			}

		})
	}

}
func TestWrapPublic(t *testing.T) {

	msg := "oh no"
	internal := "internal"
	err := WrapPublic(errors.New(internal), publicStringError(msg))

	if err.Error() != internal {
		t.Errorf("err.Error() should be '%s' but it's '%s'", internal, err.Error())
	}

	public, ok := err.(PublicError)
	if !ok {
		t.Errorf("err is not a PublicError!")
	}

	if public.PublicError() != msg {
		t.Errorf("err.PublicError() should be '%s' but it's '%s'", msg, public.PublicError())
	}
}

func TestWrap(t *testing.T) {

	var tests = []struct {
		err         error
		publicStr   string
		internalStr string
	}{
		{
			err:         Wrap(errors.New("internal"), "public"),
			publicStr:   "public",
			internalStr: "internal",
		},
		{
			err:         Wrapf(errors.New("internal"), "public %d", 5),
			publicStr:   "public 5",
			internalStr: "internal",
		},
		{
			err:         WrapPublicErr(errors.New("internal"), errors.New("public")),
			publicStr:   "public",
			internalStr: "internal",
		},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("Wrap %s %s", tt.publicStr, tt.internalStr)
		t.Run(testname, func(t *testing.T) {
			if tt.err.Error() != tt.internalStr {
				t.Errorf("InternalError is wrong. expected '%s' got '%s'", tt.internalStr, tt.err.Error())
			}
			public, ok := tt.err.(WrappedPublicError)
			if !ok {
				t.Errorf("Error is not a WrappedPublicError")
			}

			if public.PublicError() != tt.publicStr {
				t.Errorf("PublicError is wrong. expected '%s' got '%s'", tt.publicStr, public.PublicError())
			}
			if public.Cause().Error() != tt.internalStr {
				t.Errorf("CauseError is wrong. expected '%s' got '%s'", tt.internalStr, public.Cause().Error())
			}
		})
	}
}
